window.onscroll = (e) => {
    changeHeader();
}

function changeHeader() {
    let header = document.getElementsByClassName("header");
    let text = document.getElementsByClassName("dynamic");
    if (window.scrollY > 5) {
        header[0].classList.remove("active");
        for (var i = 0; i < text.length; i += 1) {
            text[i].classList.remove("active");
            text[i].classList.add("inactive");
        }
        
    }
    else {
        header[0].classList.add("active");
        for (var i = 0; i < text.length; i += 1) {
            text[i].classList.add("active");
            text[i].classList.remove("inactive");
        }
    }
}