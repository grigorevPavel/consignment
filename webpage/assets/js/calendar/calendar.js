let year_radio = document.getElementById("year");
let month_radio = document.getElementById("month");

let year = document.getElementById("dates-year");
let month = document.getElementById("dates-month");

year_radio.onclick = (e) => {
    if (year_radio.checked) {
        year.classList.remove("none");
        month.classList.add("none");
    } else {
        year.classList.add("none");
        month.classList.remove("none");
    }
}

month_radio.onclick = (e) => {
    if (month_radio.checked) {
        month.classList.remove("none");
        year.classList.add("none");
    } else {
        month.classList.add("none");
        year.classList.remove("none");
    }
}