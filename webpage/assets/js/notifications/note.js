let notes = [
    {title: "ЖК-дисплей", msg: "Изменился статус заказа", time: "21.08.21 01.22", stat: "ok"},
    {title: "Павел Григорьев", msg: "1 непрочитанное", time: "21.08.21 01.22", stat: "not-ok"},
    {title: "Новый ЖК-дисплей", msg: "Появился новый заказ", time: "21.08.21 01.22", stat: "ok"},
    {title: "Через 2 дня", msg: "Контрольная точка", time: "08.08.21", stat: "not-ok"}
];

let empty = {title: "", msg: "У вас нет уведомлений", time: ""};

let icon_bell = document.getElementById("icon-bell");
let list = document.getElementById("notes-list");

let open = false;

function toHtml(array) {
    let html_content = "";
    if (array.length) {
        for (var i = 0; i < notes.length; i += 1) {
            html_content += "<li><div class='title'>" + array[i].title + "</div><div class='msg " + array[i].stat + "'>" + array[i].msg + "</div>" + "<div class='time'>" + array[i].time + "</div></li>";
        }
    } else {
        html_content = "<li><div class='title'>" + empty.title + "</div><div class='msg'>" + empty.msg + "</div><div class='time'" + empty.time + "</div></li>";
    }
    return html_content;
}

function openClose() {
    if (open) {
        list.innerHTML = toHtml(notes);
    } else {
        list.innerHTML = "";
    }
}

window.addEventListener("mouseup", (e) => {
    if (e.target === icon_bell) {
        open = !open;
    } else {
        if (e.target !== list && open) {
            open = !open;
        }
    }
    openClose();
});