var menu_in = false;
let menu = document.getElementById("hidden-menu");
let menu_ul = document.querySelector("#hidden-menu ul");

window.addEventListener("mouseup", (e) => {
    if (e.target === document.querySelector("#hidden-button") || e.target === document.querySelector("#hidden-button i")) {
        menu_in = !menu_in;
    } else {
        if (e.target !== menu && menu_in) {
            menu_in = !menu_in;
        }
    }
    if (menu_in) {
        menu.style.right = "0px";
    } else {
        menu.style.right = "-400px";
    }
});