if (typeof window.ethereum !== 'undefined') {
    console.log('MetaMask is installed!');
    TestConnection();
    var web3 = new Web3(window.ethereum);
} else {
    console.log("Metamask is not installed");
}

async function TestConnection() {
    const accounts = await ethereum.request({ method: 'eth_accounts' });
    const account = accounts[0];
    console.log("Eth Account: " + account);
    
    if (account === undefined) {
        const eth_connect_wrapper = document.getElementById("eth-connect");
        eth_connect_wrapper.style.display = 'block';
        const btn_wrapper = document.getElementById("btn-wrapper");
        btn_wrapper.innerHTML = '<button class="btn" id="enableEthereumButton">Разрешить</button>';
        const ethereumButton = document.getElementById("enableEthereumButton");
        ethereumButton.addEventListener('click', () => {
            //Will Start the metamask extension
            Connect();
        });
    }
}

async function Connect() {
  const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
  const account = accounts[0];
  let showAccount = document.getElementById('show-account');
  showAccount.innerHTML = "Ethereum account: " + account;
  if (account !== undefined) {
    const ethConnect = document.getElementById('eth-connect');
    ethConnect.innerHTML = ""
    ethConnect.parentNode.removeChild(ethConnect);
    alert("Ethereum address: " + account);
  }
}
