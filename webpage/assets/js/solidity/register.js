async function Input() {
    let login = document.getElementById("address");
    const accounts = await ethereum.request({ method: 'eth_accounts' });
    const account = accounts[0];
    login.value = account;

    let pass = document.getElementById("password");
    let ent = document.getElementById("enter");
    let check = document.getElementById("agree-box");
    let send = document.getElementById("send-radio");
    let trans = document.getElementById("trans-radio");

    ent.addEventListener('click', () => {
        if (pass.value !== "") {
            if (!check.checked) {
                alert("Подтвердите согласие с политикой конфиденциальности");
            }
            alert('Проверьте пожалуйста выбранную роль, потом изменить ее будет невозможно\nОбновите страницу до нажатия ОК, если хотите поменять роль');
            var role = 0;
            if (send.checked) {
                role = 1;
            } else if (trans.checked) {
                role = 2;
            } else {
                role = 3;
            }
            userContract.methods.Register(pass.value, role).send({'from': accounts[0]}, (err, res)=>{
                if (err !== null) {
                    console.error(err);
                }
                console.log(res);
                window.location.replace("./home.html");
            });
        } else {
            alert('Введите пароль');
        } 
    });
}

Input();