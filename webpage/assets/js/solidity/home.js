async function Start() {
    let name = document.getElementById('name');
    let address = document.getElementById('address');
    let requsites = document.getElementById('requisites');
    const accounts = await ethereum.request({ method: 'eth_accounts' });
    const account = accounts[0];
    userContract.methods.GetName(accounts[0]).call({'from': accounts[0]}, (err, res)=>{
        if (res !== "") {
            name.innerHTML = res;
        } else {
            name.innerHTML = "Название компании можно изменить";
        }
        
    });
    userContract.methods.GetAddress(accounts[0]).call({'from': accounts[0]}, (err, res)=>{
        if (res !== "") {
            address.innerHTML = res;
        } else {
            address.innerHTML = "Адрес компании можно изменить";
        }
        
    });
    userContract.methods.GetRequisites(accounts[0]).call({'from': accounts[0]}, (err, res)=>{
        if (res !== "") {
            requsites.innerHTML = res;
        } else {
            requsites.innerHTML = "Реквизиты компании можно изменить";
        }
        
    });
}

Start();