async function Input() {
    let login = document.getElementById("address");
    const accounts = await ethereum.request({ method: 'eth_accounts' });
    const account = accounts[0];
    login.value = account;

    let pass = document.getElementById("password");
    let ent = document.getElementById("enter");

    ent.addEventListener('click', () => {
        if (pass.value !== "") {
            userContract.methods.Login(pass.value).call({'from': accounts[0]}, (err, res)=>{
                if (err !== null) {
                    console.error(err);
                }
                console.log(res);
                if (res) {
                    window.location.replace("./home.html");
                } else {
                    alert("Пароль неверный для данного адреса");
                    pass.value = "";
                }
            });
        } else {
            alert('Введите пароль');
        } 
    });
}

Input();