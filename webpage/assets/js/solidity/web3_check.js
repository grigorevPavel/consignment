if (typeof window.ethereum !== 'undefined') {
    console.log('MetaMask is installed!');
    TestConnection();
    var web3 = new Web3(window.ethereum);
} else {
    console.log("Metamask is not installed");
    window.location.replace("./index.html");
}

async function TestConnection() {
    const accounts = await ethereum.request({ method: 'eth_accounts' });
    const account = accounts[0];
    console.log("Eth Account: " + account);
    
    if (account === undefined) {
        window.location.replace("./index.html");
    }
}