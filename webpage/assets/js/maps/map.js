function initMap() {
    var opt = {
        center: {lat: 43.58118131224396, lng: 39.71826079460392},
        zoom: 15,
    };

    var map = new google.maps.Map(document.getElementById("map"), opt);
}