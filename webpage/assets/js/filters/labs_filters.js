let ordersThemeContents = [
    "Химия",
    "Физика",
    "Хиросима",
    "Фиаско",
    "Математика"
];

let ordersThemeContentsTop = [
    "Химия",
    "Физика",
    "Математика"
];

let ordersRegionContents = [
    "Москва",
    "Владивосток",
    "Мурманск",
    "Киров",
    "Нижний Новгород",
    "Санкт-Петербург"
];

let ordersRegionContentsTop = [
    "Москва",
    "Нижний Новгород",
    "Санкт-Петербург"
];

let ordersRatingContents = [
    "Отличный",
    "Хороший",
    "Средний",
    "Низкий"
];

let ordersRatingContentsTop = [
    "Отличный",
    "Хороший"
];

let ordersCollegeContents = [
    "МФТИ",
    "ВШЭ",
    "МГУ",
    "МИФИ",
    "МГТУ",
    "РАНХ и ГС"
];

let ordersCollegeContentsTop = [
    "МФТИ",
    "ВШЭ",
    "МГУ"
];

function TranformToHtml(array) {
    res = ""
    for (var i = 0; i < array.length; i += 1) {
        res += "<li class='filter_option' onclick='optionSelected(" + '"' + array[i] + '"' + ")'>" + array[i] + "</li>";
    }
    return res;
} 

let themeDynamic = document.getElementById("theme_dynamic_labs");
let regionDynamic = document.getElementById("region_dynamic_labs");

let themeInput = document.getElementById("theme_labs");
let regionInput = document.getElementById("region_labs");

let themeClear = document.getElementById("theme_clear_labs");
let regionClear = document.getElementById("region_clear_labs");

let ratingDynamic = document.getElementById("rating_dynamic_labs");
let collegeDynamic = document.getElementById("college_dynamic_labs");

let ratingInput = document.getElementById("rating_labs");
let collegeInput = document.getElementById("college_labs");

let ratingClear = document.getElementById("rating_clear_labs");
let collegeClear = document.getElementById("college_clear_labs");

themeInput.onfocus = (e) => {
    themeDynamic.innerHTML = TranformToHtml(ordersThemeContentsTop);
    themeClear.style.display = "block";
}

regionInput.onfocus = (e) => {
    regionDynamic.innerHTML = TranformToHtml(ordersRegionContentsTop);
    regionClear.style.display = "block";
}

ratingInput.onfocus = (e) => {
    ratingDynamic.innerHTML = TranformToHtml(ordersRatingContentsTop);
    ratingClear.style.display = "block";
}

collegeInput.onfocus = (e) => {
    collegeDynamic.innerHTML = TranformToHtml(ordersCollegeContentsTop);
    collegeClear.style.display = "block";
}

themeClear.onclick = (e) => {
    themeDynamic.innerHTML = "";
    themeClear.style.display = "none";
    themeInput.value = "";
}

regionClear.onclick = (e) => {
    regionDynamic.innerHTML = "";
    regionClear.style.display = "none";
    regionInput.value = "";
}

ratingClear.onclick = (e) => {
    ratingDynamic.innerHTML = "";
    ratingClear.style.display = "none";
    ratingInput.value = "";
}

collegeClear.onclick = (e) => {
    collegeDynamic.innerHTML = "";
    collegeClear.style.display = "none";
    collegeInput.value = "";
}

themeInput.oninput = (e) => {
    let userText = themeInput.value;
    if (userText != "") {
        let suggestions = ordersThemeContents.filter((ordersThemeContents) => {
            return ordersThemeContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        themeDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        themeDynamic.innerHTML = TranformToHtml(ordersThemeContentsTop);
    }
}

regionInput.oninput = (e) => {
    let userText = regionInput.value;
    if (userText != "") {
        let suggestions = ordersRegionContents.filter((ordersRegionContents) => {
            return ordersRegionContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        regionDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        regionDynamic.innerHTML = TranformToHtml(ordersRegionContentsTop);
    }
}

ratingInput.oninput = (e) => {
    let userText = ratingInput.value;
    if (userText != "") {
        let suggestions = ordersRatingContents.filter((ordersRatingContents) => {
            return ordersRatingContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        ratingDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        ratingDynamic.innerHTML = TranformToHtml(ordersRatingContentsTop);
    }
}

collegeInput.oninput = (e) => {
    let userText = collegeInput.value;
    if (userText != "") {
        let suggestions = ordersCollegeContents.filter((ordersCollegeContents) => {
            return ordersCollegeContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        collegeDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        collegeDynamic.innerHTML = TranformToHtml(ordersCollegeContentsTop);
    }
}


function optionSelected(option) {
    console.log(option);
}