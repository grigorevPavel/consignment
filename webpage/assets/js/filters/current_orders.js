let ordersThemeContents = [
    "Химия",
    "Физика",
    "Хиросима",
    "Фиаско",
    "Математика"
];

let ordersThemeContentsTop = [
    "Химия",
    "Физика",
    "Математика"
];

let ordersRegionContents = [
    "Москва",
    "Владивосток",
    "Мурманск",
    "Киров",
    "Нижний Новгород",
    "Санкт-Петербург"
];

let ordersRegionContentsTop = [
    "Москва",
    "Нижний Новгород",
    "Санкт-Петербург"
];

function TranformToHtml(array) {
    res = ""
    for (var i = 0; i < array.length; i += 1) {
        res += "<li class='filter_option' onclick='optionSelected(" + '"' + array[i] + '"' + ")'>" + array[i] + "</li>";
    }
    return res;
} 

let themeDynamic = document.getElementById("theme_dynamic_cur");
let regionDynamic = document.getElementById("region_dynamic_cur");

let themeInput = document.getElementById("theme_cur");
let regionInput = document.getElementById("region_cur");

let themeClear = document.getElementById("theme_clear_cur");
let regionClear = document.getElementById("region_clear_cur");

themeInput.onfocus = (e) => {
    themeDynamic.innerHTML = TranformToHtml(ordersThemeContentsTop);
    themeClear.style.display = "block";
}


regionInput.onfocus = (e) => {
    regionDynamic.innerHTML = TranformToHtml(ordersRegionContentsTop);
    regionClear.style.display = "block";
}

themeClear.onclick = (e) => {
    themeDynamic.innerHTML = "";
    themeClear.style.display = "none";
    themeInput.value = "";
}

regionClear.onclick = (e) => {
    regionDynamic.innerHTML = "";
    regionClear.style.display = "none";
    regionInput.value = "";
}

themeInput.oninput = (e) => {
    let userText = themeInput.value;
    if (userText != "") {
        let suggestions = ordersThemeContents.filter((ordersThemeContents) => {
            return ordersThemeContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        themeDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        themeDynamic.innerHTML = TranformToHtml(ordersThemeContentsTop);
    }
}

regionInput.oninput = (e) => {
    let userText = regionInput.value;
    if (userText != "") {
        let suggestions = ordersRegionContents.filter((ordersRegionContents) => {
            return ordersRegionContents.toLocaleLowerCase().startsWith(userText.toLocaleLowerCase());
        });
        //console.log(suggestions);
        regionDynamic.innerHTML = TranformToHtml(suggestions);
    } else {
        regionDynamic.innerHTML = TranformToHtml(ordersRegionContentsTop);
    }
}

function optionSelected(option) {
    console.log(option);
}