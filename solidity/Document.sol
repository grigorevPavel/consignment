// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;


contract Document {
   struct Consignment {
       address sender;
       address transfer;
       address getter;
       
       string port_sender;
       string port_getter;
       
       string cargo_description;
       
       bool sender_agreement;
       bool transfer_agreement;
       bool getter_agreement;
       
       string status;
       string date_send;
       string date_get;
   }
   
   mapping(address => Consignment) private consignments;
   
   address private owner;
   
   constructor() {
       owner = msg.sender;
   }
   
   function AddConignment(address _send, address _trans, address _get, 
    string memory _port_send, string memory _port_get,
    string memory _cargo, string memory _status, string memory _send_date) public payable {
        Consignment memory cons;
           cons.sender = _send;
           cons.transfer = _trans;
           cons.getter = _get;
           
           cons.port_sender = _port_send;
           cons.port_getter = _port_get;
           cons.cargo_description = _cargo;
           
           cons.sender_agreement = true;
           cons.getter_agreement = false;
           cons.transfer_agreement = false;
           
           cons.status = _status;
           cons.date_send = _send_date;
           cons.date_get = "";
   }
}
