// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;

contract Users {

    enum Role {
        NONE,
        SENDER, 
        TRANSFER, 
        GETTER 
    }

    struct User {
        Role role;
        string name;
        string addr;
        string index;
        string requisites;
        bytes32 password_hash;
        bool registered;
    }
    
    mapping(address => User) private users;
    
    address private owner;
    
    constructor() {
        owner = msg.sender;
    }
    
    function Login(string memory _password) public view returns(bool) {
        return users[msg.sender].password_hash == keccak256(abi.encodePacked(_password));
    }
    
    function Register(string memory _password, uint16 _role) public payable {
        if (!users[msg.sender].registered) {
            users[msg.sender].password_hash = keccak256(abi.encodePacked(_password));
            
            if (_role == 1) {
                users[msg.sender].role = Role.SENDER;
            } else if (_role == 2) {
                users[msg.sender].role = Role.TRANSFER;
            } else {
                users[msg.sender].role = Role.GETTER;
            }
            
            users[msg.sender].name = "";
            users[msg.sender].addr = "";
            users[msg.sender].index = "";
            users[msg.sender].requisites = "";
            users[msg.sender].registered = true;
        }
    }
    
    function GetRole(address _user) public view returns(uint16) {
        if (users[_user].role == Role.SENDER) {
            return 1;
        } else if (users[_user].role == Role.TRANSFER) {
            return 2;
        } else if (users[_user].role == Role.GETTER) {
            return 3;
        } else {
            return 0;
        }
    }
    
    function GetName(address _user) public view returns(string memory) {
        return users[_user].name;
    }
    
    function GetAddress(address _user) public view returns(string memory) {
        return users[_user].addr;
    }
    
    function GetRequisites(address _user) public view returns(string memory) {
        return users[_user].requisites;
    }
    
    function GetIndex(address _user) public view returns(string memory) {
        return users[_user].index;
    }
    
     function GetOwner() public view returns(address) {
        return owner;
    }
}